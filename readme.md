# About

This repository and Wiki are for HiG IMT3612 course: GPU Programming.
The course follows the Udacity CS344 course and the default CUDA code
examples. We will try to keep it flexible so that typical usage patterns 
can be tested with OpenCL equivalent code. 


# Course material

## Setup

[CUDA and NVidia setup materials](https://developer.nvidia.com/cuda-downloads)

Help in setting up your CUDA programming environments and download the most recent drivers for your GPUs.

[AMD setup material](http://developer.amd.com/tools-and-sdks/opencl-zone/amd-accelerated-parallel-processing-app-sdk/)

Drivers and SDK for AMD GPUs. 

[Android SDK for NVidia CUDA for Tegra K1](https://developer.nvidia.com/tegra-android-development-pack)

Tegra Android development toolkit.





# [Udacity Course](https://www.udacity.com/course/cs344)

This is the main source of lecture materials that this course follows. We will follow this course, with each segment matching the course week in Gjovik. 



# [CUDA Training Zone](http://developer.nvidia.com/cuda-training)

This is the excellent source of additional course materials and additional lectures on various topics related to GPU programming. We will provide more detailed resources as the course progresses, in relation to individual topics that we cover in the lectures. 





# [OpenCL Training Zone](developer.amd.com/tools-and-sdks/opencl-zone/opencl-resources/opencl-course-introduction-to-opencl-programming/)

Introduction to OpenCL

